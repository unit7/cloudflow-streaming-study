import Dependencies._

ThisBuild / scalaVersion := Settings.ScalaVersion.It
ThisBuild / organization := "com.breezzo"
ThisBuild / organizationName := "Streaming study"
ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

lazy val log4jConfig = file("pipeline/src/main/resources/log4j2.xml")
lazy val root = (project in file("."))
  .settings(
    publish / skip := true,
    name := "streaming-study",
    libraryDependencies += Testing.ScalaTest % Test
  )
  .aggregate(
    pipeline,
    pagesRestIngress,
    topPagesCalculator
  )

lazy val pipeline = (project in file("pipeline"))
  .withId(pipelineName)
  .enablePlugins(CloudflowApplicationPlugin)
  .settings(
    publish / skip := true,
    blueprint := Some(s"blueprint.conf"),
    name := pipelineName,
    runLocalConfigFile := Some((baseDirectory.value / localConfigPath).getAbsolutePath)
  )
  .dependsOn(pagesRestIngress, topPagesCalculator)

lazy val datamodel = newRootModule("datamodel")
  .enablePlugins(CloudflowLibraryPlugin)
  .settings(
    publish / skip := true,
    Compile / unmanagedSourceDirectories += baseDirectory.value / "target/scala-2.12/scala_avro",
    Settings.SuppressWarnSettings,
    version := "0.0.1",
    organization := "com.breezzo",
    sourceGenerators in Compile += (avroScalaGenerateSpecific in Compile).taskValue,
    mappings in (Compile, packageSrc) ++= (managedSources in Compile).value map (s => (s, s.getName))
  )

lazy val pagesRestIngress = newRootModule("pages-rest-ingress")
  .enablePlugins(CloudflowAkkaPlugin)
  .dependsOn(datamodel)
  .settings(
    libraryDependencies ++= Logging.AllRuntime
  )

lazy val topPagesCalculator = newRootModule("top-pages-calculator")
  .enablePlugins(CloudflowFlinkPlugin)
  .dependsOn(datamodel)
  .settings(
    libraryDependencies ++= Logging.AllRuntime
  )

val pipelineName    = "streaming-study-pipe"
val localConfigPath = "src/main/resources/local.conf"

def newRootModule(moduleID: String): Project = {
  newModule(moduleID, moduleID)
}

def newModule(moduleID: String, path: String): Project = {
  Project(id = moduleID, base = file(path))
    .settings(name := moduleID)
    .withId(moduleID)
    .settings(
      Settings.CommonSettings,
      Compile / resources += log4jConfig,
      publish / skip := true
    )
}
