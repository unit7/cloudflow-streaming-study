package com.breezzo.streamlet

import cloudflow.flink.{ FlinkStreamlet, FlinkStreamletLogic }
import cloudflow.streamlets.StreamletShape
import cloudflow.streamlets.avro.{ AvroInlet, AvroOutlet }
import com.breezzo.data.{ PageClick, TopPagesLastFiveMinutes }
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

class TopPagesCalculator extends FlinkStreamlet {
  @transient private val in  = AvroInlet[PageClick]("in")
  @transient private val out = AvroOutlet[TopPagesLastFiveMinutes]("out")

  override def shape(): StreamletShape = StreamletShape(in, out)

  override protected def createLogic(): FlinkStreamletLogic = new FlinkStreamletLogic() {
    override def buildExecutionGraph(): Unit = {
      context.env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

      val clicks = readStream(in).assignAscendingTimestamps(_.timestamp.toEpochMilli)

      val topPagesStream = clicks
        .keyBy(_.url)
        .window(TumblingEventTimeWindows.of(Time.minutes(5)))
        .aggregate(new CountClicksFunction)
        .timeWindowAll(Time.minutes(5))
        .process(new TakeTopPagesFunction(5))

      topPagesStream.print()

      writeStream(out, topPagesStream)
    }
  }
}
