package com.breezzo.streamlet

import com.breezzo.data.{ PageClick, PageClicks, TopPagesLastFiveMinutes }
import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.streaming.api.scala.function.ProcessAllWindowFunction
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

class CountClicksFunction extends AggregateFunction[PageClick, PageClicks, PageClicks] {
  override def createAccumulator(): PageClicks = new PageClicks()

  override def add(value: PageClick, accumulator: PageClicks): PageClicks =
    accumulator.copy(page = value, clicks = accumulator.clicks + 1)

  override def getResult(accumulator: PageClicks): PageClicks = accumulator

  override def merge(a: PageClicks, b: PageClicks): PageClicks = a.copy(clicks = a.clicks + b.clicks)
}

class TakeTopPagesFunction(pagesCount: Int)
  extends ProcessAllWindowFunction[PageClicks, TopPagesLastFiveMinutes, TimeWindow] {
  private implicit val pagesOrdering: Ordering[PageClicks] = Ordering.by[PageClicks, Int](_.clicks).reverse

  override def process(
                        context: Context,
                        elements: Iterable[PageClicks],
                        out: Collector[TopPagesLastFiveMinutes]
                      ): Unit = {
    val topPages = elements.toSeq.sorted.take(pagesCount)
    out.collect(TopPagesLastFiveMinutes(topPages))
  }
}
