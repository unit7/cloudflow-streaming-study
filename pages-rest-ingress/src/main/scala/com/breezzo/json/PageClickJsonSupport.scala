package com.breezzo.json

import com.breezzo.data.PageClick
import spray.json.DefaultJsonProtocol

object PageClickJsonSupport extends DefaultJsonProtocol with InstantJsonSupport with Serializable {
  implicit val pageClickFormat = jsonFormat3(PageClick.apply)
}
