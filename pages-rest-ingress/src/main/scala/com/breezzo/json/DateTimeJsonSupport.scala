package com.breezzo.json

import java.time.{ Instant, LocalDate }

import spray.json.{ deserializationError, DefaultJsonProtocol, JsNumber, JsValue, JsonFormat }

trait InstantJsonSupport extends DefaultJsonProtocol {
  implicit object InstantFormat extends JsonFormat[Instant] {
    def write(instant: Instant): JsNumber = JsNumber(instant.toEpochMilli)

    def read(json: JsValue): Instant = json match {
      case JsNumber(value) ⇒ Instant.ofEpochMilli(value.toLong)
      case other           ⇒ deserializationError(s"Expected Instant as JsNumber, but got: $other")
    }
  }
}

trait LocalDateJsonSupport extends DefaultJsonProtocol {
  implicit object LocalDateFormat extends JsonFormat[LocalDate] {
    def write(date: LocalDate): JsNumber = JsNumber(date.toEpochDay)

    def read(json: JsValue): LocalDate = json match {
      case JsNumber(value) ⇒ LocalDate.ofEpochDay(value.toInt)
      case other           ⇒ deserializationError(s"Expected LocalDate as JsNumber, but got: $other")
    }
  }
}
