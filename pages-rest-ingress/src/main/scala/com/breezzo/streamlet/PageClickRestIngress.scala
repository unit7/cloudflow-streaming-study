package com.breezzo.streamlet

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import cloudflow.akkastream.util.scaladsl.HttpServerLogic
import cloudflow.akkastream.{ AkkaServerStreamlet, AkkaStreamletLogic }
import cloudflow.streamlets.StreamletShape
import cloudflow.streamlets.avro.AvroOutlet
import com.breezzo.data.PageClick
import com.breezzo.json.PageClickJsonSupport._

class PageClickRestIngress extends AkkaServerStreamlet {
  private val out = AvroOutlet[PageClick]("out", _.url)

  def shape: StreamletShape = StreamletShape(out)

  def createLogic: AkkaStreamletLogic = new HttpServerLogic(this) {
    final override def route(): Route =
      path("page-click") {
        post {
          entity(as[PageClick]) { click =>
            val sink = sinkRef(out)
            onSuccess(sink.write(click)) { _ =>
              complete(StatusCodes.OK)
            }
          }
        }
      }
  }
}
