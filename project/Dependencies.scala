import sbt._

object Dependencies {

  object Logging {
    lazy val Slf4j = "com.typesafe.akka" %% "akka-slf4j" % "2.5.29"
    lazy val Disruptor = "com.lmax" % "disruptor" % "3.4.2" % "runtime"
    lazy val Logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
    lazy val AllRuntime = Seq(Logback, Slf4j, Disruptor)
  }

  object Testing {
    val ScalaTest = "org.scalatest" %% "scalatest" % "3.2.0"
  }
}
